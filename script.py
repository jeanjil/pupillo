#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Copyright 2022 Aurore Duchamps & Jean-Jil Duchamps

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import numpy as np
import matplotlib.pyplot as plt

## Paramètres du script

# Noms des fichiers
f_pupil_measures = 'pupil_positions.csv'
f_annotations = 'annotations.csv'  # mettre None si pas de fichier

# Pour le lissage des courbes
freq_sampling = 10   # nombre de points par seconde dans les courbes finales
smooth_window = 0.4  # fenêtre de lissage, en secondes
                     # (ne pas choisir en dessous de 2/freq_sampling)

# Paramètres du nettoyage des données (problèmes de mesure, clignements d'yeux)
conf_threshold = 0.7  # seuil de confiance acceptable
quantile = 0.05       # on élimine quantile*100 % des valeurs extrèmes
margin = 0.1          # mais on garde les valeurs qui s'approchent des valeurs
                      # retenues, dans une marge donnée par ce paramètre

# Forcer la détection des crêtes sur un seul œil, ou sur la moyenne des deux
use_eyes = 'auto'  # valeurs possibles : 'left', 'right', 'mean', 'auto'
suppressed_threshold = 0.1  # si use_eyes = 'auto', détermine si l'on laisse
                            # tomber un œil si jamais on doit supprimer trop de
                            # valeurs

# Détection des cretes (True active la détection, False -> affichage simple)
detect_cretes = True


## Paramètres si detect_cretes = True

# Nom du fichier pour exportation (`f_export = None` pour annuler)
f_export = 'exportations.csv'  # utilisé seulement si cretes = True

# Délai à prendre en compte pour la baseline (en secondes)
base_delay = 0.1

# Position de la ligne correspondant à une crête (début, milieu ou fin)
pos_crete = 'end' # valeurs : 'beg', 'mid', 'end'


## Lecture des données

def read_data(f_pupil_measures, f_annotations=None):
    """
    Lit les données fournies.

    Paramètres
    ----------
    f_pupil_measures : str
        Nom du fichier de mesure des dilatations de la pupille (format CSV).
    f_annotations : str
        Nom du fichier des annotations (format CSV).

    Renvoie
    -------
    Un tuple (timestamps, eye_ids, confidence, diameters, annotations), avec
    timestamps : numpy array
        Array des timestamps des mesures.
    eye_ids : numpy array
        Array des identification (œil droit, œil gauche) des yeux.
    confidence : numpy array
        Array contenant la confiance de chaque mesure.
    diameters : numpy array
        Array contenant chacune des mesures de dilatation de pupille.
    annotations : list
        Contient les timestamps de chaque annotation.
    """
    timestamp_list = []
    eye_id_list = []
    confidence_list = []
    diameter_list = []
    annotations = []

    with open(f_pupil_measures) as f:
        print(f'## Lecture du fichier "{f_pupil_measures}"')
        # on ignore la première ligne (qui contient les en-têtes de chaque
        # colonne)
        f.readline()

        for line in f:
            # on parse le CSV, selon le format attendu :
            # - colonne 0 : timestamp
            # - colonne 2 : eye_id (œil gauche ou droit)
            # - colonne 3 : confiance de la mesure
            # - colonne 6 : diamètre de dilatation de la pupille
            line_split = line.split(',')
            timestamp_list.append(float(line_split[0]))
            eye_id_list.append(float(line_split[2]))
            confidence_list.append(float(line_split[3]))
            diameter_list.append(float(line_split[6]))
    timestamps = np.array(timestamp_list)

    try:
        with open(f_annotations) as f:
            print(f'## Lecture du fichier "{f_annotations}"')
            # on fait la même opération qu'avant
            f.readline()

            for line in f:
                line_split = line.split(',')
                annotations.append(float(line_split[1]) - timestamps[0])
    except:
        print('## Pas de fichier d\'annotations.')

    return (timestamps,
            np.array(eye_id_list),
            np.array(confidence_list),
            np.array(diameter_list),
            annotations)


## Nettoyage des données

class ValeursAberrantes(Exception):
    pass

def clean_data(data, quantile=0.05, margin=0.1, conf_threshold=0.9,
               use_eyes='auto', suppressed_threshold=0.1):
    """
    On enlève les valeurs aberrantes des listes : celles où la confiance
    est en dessous du seuil fixé, et celles où le diamètre est aberrant malgré
    une confiance forte (erreurs de mesure)

    Paramètres
    ----------
    data : tuple
        Doit correspondre aux données renvoyées par la fonction `read_data`.
    quantile : float, optionnel
        Permet d'éliminer quantile*100 % des valeurs extrèmes. Défaut : 0.05.
    margin : float, optionnel
        Mais on va rajouter les valeurs qui s'approchent des valeurs retenues,
        dans une marge donnée par ce paramètre. Défaut : 0.1.
    conf_threshold : float, optionnel
        Seuil de confiance acceptable. Défaut : 0.9.
    use_eyes : str, optionnel
        Permet de forcer la détection des crêtes sur un seul œil, ou sur la
        moyenne des deux. Valeurs possibles :
            - 'auto' : laisse le programme choisir,
            - 'left' : œil gauche,
            - 'right' : œil droit,
            - 'mean' : moyenne des deux.
        Défaut : 'auto'.
    suppressed_threshold : float, optionnel
        Utilisé dans le cas où `use_eyes` vaut 'auto'. Si tel ou tel œil
        présente une proportion de valeurs aberrantes plus grande que ce
        paramètre, on le retire de l'analyse. Défaut : 0.1.

    Soulève
    -------
    ValeursAberrantes
        Si l'on a trop de valeurs aberrantes, le programme s'arrête en
        soulevant cette exception.

    Renvoie
    -------
    Un tuple (timestamps, eye_ids, diameters, annotations, kept_eyes), où l'on
    a retiré les points aberrants. On décale aussi les valeurs de l'array
    timestamps pour les faire démarrer à 0. La valeur `kept_eyes` vaut soit
    'mean', soit 'left', soit 'right' en fonction des yeux sélectionnés.
    """
    (timestamps, eye_ids, confidence, diameters, annotations) = data

    high = np.quantile(diameters, 1-quantile/2)
    low = np.quantile(diameters, quantile/2)
    high, low = (high + margin/2 * (high-low), low - margin/2 * (high-low))
    condition = (confidence > conf_threshold) \
                & (diameters > low) & (diameters < high)

    suppressed_total = 1-sum(condition) / len(timestamps)
    suppressed_left = 1-sum(condition & (eye_ids == 0)) / sum(eye_ids == 0)
    suppressed_right = 1-sum(condition & (eye_ids == 1)) / sum(eye_ids == 1)

    print('## Nettoyage des données')

    # Affichage d'informations
    left_infos = f'L\'œil gauche présente {100*suppressed_left:.3} % de valeurs aberrantes'
    right_infos = f'L\'œil droit présente {100*suppressed_right:.3} % de valeurs aberrantes'
    kept_eyes = use_eyes if use_eyes != 'auto' else  'mean'
    if use_eyes != 'right':
        if suppressed_left > suppressed_threshold:
            print('/!\\ Warning : ', end='')
        print(left_infos, end='')
        if suppressed_left > suppressed_threshold and use_eyes == 'auto':
            print(' (ignoré)')
            kept_eyes = 'right'
        else:
            print()
    if use_eyes != 'left':
        if suppressed_right > suppressed_threshold:
            print('/!\\ Warning : ', end='')
        print(right_infos, end='')
        if suppressed_right > suppressed_threshold and use_eyes == 'auto':
            print(' (ignoré)')
            if kept_eyes == 'right':
                raise ValeursAberrantes('Trop de valeurs aberrantes !')
            kept_eyes = 'left'
        else:
            print()

    if kept_eyes == 'mean':
        print(f'On ignore {100*suppressed_total:.3} % des valeurs totales')

    return (timestamps[condition] - timestamps[0],
            eye_ids[condition],
            diameters[condition],
            annotations,
            kept_eyes)


## Lissage des courbes
# On lisse les données en faisant des moyennes toutes bêtes, et en interpolant
# si besoin.
# TODO : cette fonction n'est pas codée de manière optimale... Idéalement,
# remplacer ça par une fonction standard d'une librairie...

def smooth(tnew, t, y, window=0.4):
    """
    Lisse des données.

    Paramètres
    ----------
    tnew : numpy array
        Nouveaux points où l'on veut interpoler les valeurs de y.
    t : numpy array
        Points où l'on connaît les valeurs de y.
    y : numpy array
        Courbe à lisser.
    window : float, optionnel
        Largeur de la fenêtre de lissage, en secondes. Défaut : 0.4.

    Renvoie
    -------
    ynew : numpy array
        Valeurs moyennées de y (et interpolées) correspondantes aux points de
        temps de tnew.
    """
    ynew = np.zeros_like(tnew)
    for (i, t_curr) in enumerate(tnew):
        y_kept = y[(t > t_curr - window/2) & (t < t_curr + window/2)]
        if len(y_kept) == 0: # linear interpolation
            t_a = t[t < t_curr]
            t_b = t[t > t_curr]
            if len(t_a) == 0 or len(t_b) == 0:
                ynew[i] = np.nan
                continue
            j, t_a, t_b = (len(t_a)-1), max(t_a), min(t_b)
            theta = (t_curr - t_a) / (t_b - t_a)
            ynew[i] = y[j] * theta + y[j+1] * (1-theta)
        else:
            ynew[i] = np.mean(y_kept)
    return ynew

def smooth_data(data, freq_sampling=10, window=0.4):
    """
    Lisse les données après nettoyage.

    Paramètres
    ----------
    data : tuple
        Un tuple comme renvoyé par `clean_data`.
    freq_sampling : int, optionnel
        Nombre de points par seconde dans les courbes à afficher.
        Défaut : 10.
    window : float, optionnel
        Largeur de la fenêtre de lissage, en secondes. Défaut : 0.4.

    Renvoie
    -------
    Un tuple (tnew, left, right, annotations), avec
        - tnew : numpy array
            Un vecteur de temps échantillonés à intervalles réguliers.
        - left et right : numpy array
            Soit None (si non pertinent), soit le lissage des données pour
            l'œil gauche et l'œil droit.
        - annotations : list
            Reste la liste initiale des annotations.
    """
    timestamps, eye_ids, diameters, annotations, kept_eyes = data
    step = 1/freq_sampling
    tnew = np.arange(timestamps.min(), timestamps.max()+step, step)
    left = right = None
    if kept_eyes != 'right':
        left = smooth(tnew, timestamps[eye_ids == 0],
                      diameters[eye_ids == 0], window)
    if kept_eyes != 'left':
        right = smooth(tnew, timestamps[eye_ids == 1],
                       diameters[eye_ids == 1], window)
    return (tnew, left, right, annotations)


## Repérage (semi-)automatique des crêtes

def cretes(data, pos_crete='end', base_delay=0.1):
    """
    Pour une série temporelle de valeurs de dilatation de pupille, renvoie une
    liste des "crêtes" détectées, i.e. une liste de tuples
    (t, high, low, d_norm) avec t le temps d'atteinte d'une crête (un pic dans
    la dilatation de la pupille), high sa plus haute valeur, low sa plus basse
    valeur, et d_norm la valeur delta normalisée :
        rapport (high-low) / (maximum global / minimum global).

    Parameters
    ----------
    data : tuple
        Un tuple comme renvoyé par `smooth_data`.
    pos_crete : str, optionnel
        Détermine quand enregistrer la position temporelle de la crête. Valeurs
        possibles :
            - 'beg' : début de la crête
            - 'mid' : milieu de la crête
            - 'end' : fin de la crête
        Défaut : 'end'.
    base_delay : float, optionnel
        Délai (en seconde) à prendre en compte pour le calcul de la valeur
        basse (baseline). Si ce délai est >0, on calcule une moyenne, sinon on
        prend la valeur minimale (au début de la crête).
        Attention, dans le cas `base_delay=0`, on risque de détecter beaucoup
        de crêtes (micro-variations, qui ne sont pas considérées comme des
        crêtes si l'on moyenne la baseline).
        Défaut : 0.1.

    Renvoie
    -------
    list_cretes : list
        Liste des crêtes.
    """
    # Récupère la liste des temps dans `data`, et détermine sur quelle liste
    # (x_list) faire la détection des crêtes.
    t_list, left, right, _ = data
    if left is None:
        x_list = right
    elif right is None:
        x_list = left
    else:
        x_list = (left + right) / 2

    list_end = []
    list_beg = []
    list_high = []
    list_low = []
    crete = False
    x_prev, t_prev = x_list[0], t_list[0]
    delta_ref = np.nanmax(x_list) - np.nanmin(x_list)
    for t, x in zip(t_list[1:], x_list[1:]):
        if not crete and x > x_prev:
            crete = True
            list_low.append(x_prev)
            list_beg.append(t)
        elif crete and x < x_prev:
            crete = False
            list_high.append(x_prev)
            list_end.append(t_prev)
        x_prev, t_prev = x, t
    list_cretes = []
    for beg, tend, h, l in zip(list_beg, list_end, list_high,list_low):
        if base_delay > 0:
            baseline = np.mean(x_list[(t_list > beg - base_delay) & (t_list <= beg)])
        else:
            baseline = l
        if baseline >= h:
            continue
        if pos_crete == 'beg':
            t = beg
        elif pos_crete == 'mid':
            t = (beg + tend) / 2
        else: # 'end'
            t = tend
        delta_norm = (h-baseline) / delta_ref
        list_cretes.append((t, h, baseline, delta_norm))
    return list_cretes


## Exportation des données de crêtes

def export_cretes(file, list_annot, list_cretes):
    """
    Exporte les valeurs calculées pour les crêtes au format CSV.
    Marque certaines crêtes si elles semblent correspondre à une annotation.

    Parameters
    ----------
    file : str
        Nom du fichier.
    list_annot : list
        Liste d'annotations (liste de temps).
    list_cretes : list
        Liste comme renvoyée par la fonction `cretes`.
    """
    with open(file, 'w') as f:
        f.write('t (s),annotation,creux,crete,delta,delta normalise\n')
        i_annot = 0
        for t, h, b, dn in list_cretes:
            while i_annot < len(list_annot) and list_annot[i_annot] < t:
                f.write(f'{list_annot[i_annot]},x\n')
                i_annot += 1
            f.write(f'{t},,{b},{h},{h-b},{dn}\n')
        while i_annot < len(list_annot):
            f.write(f'{list_annot[i_annot]},x\n')
            i_annot += 1


## Affichage graphique

def affichage(data):
    """
    Affiche les courbes de pupillométrie avec pyplot.

    Paramètres
    ----------
    data : tuple
        Un tuple comme renvoyé par `smooth_data`.
    """
    tnew, left, right, annotations = data
    plt.clf()
    a = b = None
    if left is not None:
        plt.plot(tnew, left, label='Œil gauche')
        a, b = np.nanmin(left), np.nanmax(left)
    if right is not None:
        plt.plot(tnew, right, label='Œil droit')
        a1, b1 = np.nanmin(right), np.nanmax(right)
        a, b = (min(a1, a), max(b1, b)) if a is not None else (a1, b1)
    # Élargit un peu la fenêtre d'affichage verticalement (pas très propre...).
    a, b = a - 0.05 * (b-a), b + 0.05 * (b-a)
    if annotations:
        for t in annotations:
            line, = plt.plot([t, t], [a, b], color = 'C4')
        line.set_label('Annotations')
    plt.legend()
    plt.title('Dilatation des pupilles au cours du temps')
    plt.xlabel('Temps (s)')
    plt.ylabel('Dilatation ($10^{-4}$m)')
    plt.show()


def affichage_cretes(data, list_cretes):
    """
    Affiche les courbes de pupillométrie avec pyplot, et ajoute les crêtes
    calculées.

    Paramètres
    ----------
    data : tuple
        Un tuple comme renvoyé par `smooth_data`.
    list_cretes : list
        Une liste, comme renvoyée par `cretes`.
    """
    tnew, left, right, annotations = data
    plt.clf()
    if left is None:
        ref = right
        lab = 'Œil droit'
    elif right is None:
        ref = left
        lab = 'Œil gauche'
    else: # 'mean'
        ref = (left + right) / 2
        lab = 'Moyenne de la dilatation des pupilles'
    plt.plot(tnew, ref, label=lab)
    a, b = np.nanmin(ref), np.nanmax(ref)
    # Élargit un peu la fenêtre d'affichage verticalement (pas très propre...).
    a, b = a - 0.05 * (b-a), b + 0.05 * (b-a)
    if annotations:
        for t in annotations:
            line, = plt.plot([t, t], [a, b], color = 'C4')
        line.set_label('Annotations')
    for t, h, b, dn in list_cretes:
        line, = plt.plot([t, t], [b, h], color = 'C3')
        plt.text(t, b, f' {round(b,2)}')
        plt.text(t, h, f' {round(h,2)}')
        plt.text(t, (b+h)/2, f' δ={round(h-b,2)} → {round(dn*100,2)} %')
    line.set_label('Crêtes')
    plt.legend()
    plt.title('Dilatation des pupilles au cours du temps')
    plt.xlabel('Temps (s)')
    plt.ylabel('Dilatation ($10^{-4}$m)')
    plt.show()


## Fonction principale du script

if __name__ == '__main__':
    # Lecture et nettoyage des données.
    data = read_data(f_pupil_measures, f_annotations)
    data = clean_data(data, quantile, margin, conf_threshold,
                      use_eyes, suppressed_threshold)
    data = smooth_data(data, freq_sampling, smooth_window)
    print('## Affichage graphique')
    if detect_cretes:
        list_cretes = cretes(data, pos_crete, base_delay)
        affichage_cretes(data, list_cretes)
        if f_export is not None:
            print(f'## Exportation dans le fichier "{f_export}"')
            export_cretes(f_export, data[3], list_cretes)
    else:
        affichage(data)
